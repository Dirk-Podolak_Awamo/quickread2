import unittest
import urllib2

class TestDeployment(unittest.TestCase):

    def setUp(self):
        pass

    def test_deployment(self):
        url = "http://localhost:8080"
        try:
            result = urllib2.urlopen(url)
            pass # assuming no error happened
        except urllib2.URLError, e:
            self.fail("couldn't find running app at " + url)

if __name__ == '__main__':
    unittest.main()

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// --- handler stuff
// ---------------------------------------------------------------------------------------------------------------------------------------------------
var dirty = false;

var setPersistenceHandler = function() {
    $('#nodeTitle').keyup(onKeyUpHandler);
    $('#nodeText').keyup(onKeyUpHandler);
};

var onKeyUpHandler = function() {
    dirty = true;
    afterKeyUpTimer.play(true); // reset
};
// ---------------------------------------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// --- timer stuff
// ---------------------------------------------------------------------------------------------------------------------------------------------------
// 3sec timer, it will save the newInfoQueue 3sec after last keyUp event
var afterKeyUpTimer = $.timer(function() {
    console.log('save now after key up');
    save(selected_index);
    // restart itself
    afterTimeout.play(true); // reset
});
// set timeout to 3 seconds and start
afterKeyUpTimer.set({ time : 3000 });

// 15sec timer, it will save the newInfoQueue every 15sec
var afterTimeout = $.timer(function() {
    console.log('save now after timeout');
    save(selected_index);
    // restart itself
    afterTimeout.play(true); // reset
});
// set timeout to 15 seconds and start
afterTimeout.set({ time : 15000, autostart : true });
// ---------------------------------------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// --- save stuff
// ---------------------------------------------------------------------------------------------------------------------------------------------------
var save = function(index) {
    if (dirty) {
        doAction($('#edit-form'), function(obj) {
            if (obj.success) {
                var content_node = JSON.parse(obj.data);
                var $selected_row = $('tr[data-tt-id="' + index + '"]');
                var $selected_cell = $selected_row.find('td > span.title');
                $selected_row.data('text', content_node.content.text);
                $selected_cell.text(content_node.content.title);

                $.pnotify({
                    title: 'Erfolg!',
                    text: 'Änderung gespeichert',
                    type: 'success',
                    width: '100%',
                    addclass: 'stack-bar-bottom',
                    stack: stack_bar_bottom
                });
            } else {
                $.pnotify({
                    title: 'Warnung!',
                    text: 'Änderung konnte nicht gespeichert werden!',
                    type: 'error',
                    width: '100%',
                    addclass: 'stack-bar-bottom',
                    stack: stack_bar_bottom
                });
            }
        });
        dirty = false;
    }
    afterKeyUpTimer.stop();
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------
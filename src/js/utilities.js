var createAutoClosingAlert = function(selector, delay) {
    $(selector).removeClass('hide');
    $(selector).delay(delay).fadeOut("slow", function () {
        $(this).addClass('hide');
        $(this).show();
    });
};

var getData = function($actor) {
    var data = null;
    
    if ($actor.is('form')) {
        data = $actor.serialize();
    } else {
        data = JSON.stringify($actor.data());
    }
    
    return data;
};

var doPost = function(url, operation, data, callback) {
    $.post(url, { 'operation': operation, 'data': data })
        .done(function(result) {
            var obj = JSON.parse(result);
            $('#internal_content').html('<p>the server returned the success value ' + obj.success + ' at ' + new Date() + '</p>');

            if (callback) {
                callback(obj);
            }
        });
};

var doGet = function(url, operation, data) {
    $.getJSON(url + '?operation=' + operation + '&data=' + data, function(result) {
        var html = '<p>the server returned the success value ' + result.success + ' at ' + new Date() + '</p>\n';
        var dataObject = JSON.parse(result.data);
        $.each(dataObject, function(key, value){
            html += get_html(value);
        });
        $('#internal_content').html(html);
    });
};

var executeAction = function($actor, callback) {
    var url       = $actor.data('url') || '/action';
    var operation = $actor.data('operation');
    var method    = $actor.data('method') || 'post';
    var data      = getData($actor);
    
    if ('post' === method) {
        doPost(url, operation, data, callback);
    } else if ('get' === method) {
        doGet(url, operation, data);
    }
};

var doAction = function($actor, callback) {
    var withConfirmation = $actor.data('confirm');
    
    if (withConfirmation) {
        $('#confirmYesButton').data('actor', $actor);
        $('#confirmYesButton').data('callback', callback);
        $('#confirmYesButton').on("click", confirmationClick);
    } else {
        executeAction($actor, callback);
    }
};

var confirmationClick = function() {
    $('#confirmYesButton').off("click", confirmationClick);
    $actor = $(this).data('actor');
    callback = $(this).data('callback');
    executeAction($actor, callback);
};

$(document).ready(function() {
    $('#confirmLabel')    .html('Löschen bestätigen');
    $('#confirmText')     .html('Diesen Knoten wirklich löschen?');
    $('#confirmNoButton') .html('Nein, abbrechen!');
    $('#confirmYesButton').html('Ja, löschen!');

    $('#confirm').on('show.bs.modal', function(e) {
        $('#confirmNoButton').click(function(e) { return true; });
    });
});
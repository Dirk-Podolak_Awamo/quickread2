# Übersicht #

Die Anwendung ist in Python geschrieben, läuft in der Google App Engine (GAE) und verwendet deshalb das Web Framework webapp2. Als Templating Engine kommt Jinja2 zum Einsatz.
Während der Entwicklung habe ich ein Blog dazu geschrieben, siehe [hier](http://quickread-de.blogspot.de/).

### Einstieg ###

Die Anwendung ist eine Webanwendung nach MVC Pattern. Anfragen per HTTP werden mit einem Router zum jeweiligen Controller (hier handler genannt) geroutet. Dieser rendert
die View und liefert sie an den Anfragenden zurück. Daten des Modells werden per Python NDB Datastore in der GAE gespeichert.
Im folgenden sind die wichtigsten Klassen und Konzepte vorgestellt, detailliertere Informationen sind in den einzelnen Dateien selbst zu finden.

- Dispatcher/Router
	Die Datei main.py routet Anfragen zum entsprechenden handler. Momentan gibt es einen Handler für Tests, einen für Administratives und einen Main Handler für alles andere.
- Handler
	Alle Handler liegen unter "handler". Sie sind immer von webapp2.RequestHandler abgeleitet. Um die Verwendung von Jinja2 zu vereinfachen habe ich den jinja_worker
	geschrieben, der von webapp2.RequestHandler erbt und ein paar Jinja2-spezifische Helper bereitstellt. Alle eigenen Handler erben deswegen direkt von
	jinja_worker.Handler_jinja_worker.
- Templates
	Alle HTML Templates liegen unter "templates". Es gibt ein paar Standardtemplates, die fast überall verwendet werden, wie Navigation (navigation.top) oder Footer (footer).
- Model
	Unter "model" liegen die Dateien "acl_node" und "content_node". Ein ACL-Node (access control list) speichert Zugriffsberechtigungen, ein Content Node Inhalte.
	Ein ACL Node regelt immer die Zugriffsrechte eines Benutzers auf einen Content Node. Er enthält daher einen Verweis auf den Benutzer, für dessen Zugriffsrechte er
	zuständig ist, einen Verweis auf den Content Node, der verwaltet wird. Es gibt Lese- und Schreibrecht, die separat vergeben werden können. Es gibt außerdem ein
	Ausführungsrecht, das aber zur Zeit noch nicht verwendet wird.
	Ein Content Node speichert einen JSON String ab, so daß beliebige Datenstrukturen abgespeichert werden können. Ein Content Node hat außerdem eine Liste seiner Kind
	Content Nodes. Alle Content Nodes eines Benutzers sind hierarchisch in einer Baumstruktur abgespeichert. Der Wurzelknoten ist der sogenannte private Knoten des
	Benutzers. Dieser ist für den Benutzer immer sichtbar und kann nicht gelöscht werden. Er wird angelegt, wenn sich der Benutzer zum ersten Mal anmeldet.
	Die Datei "list_data" implementiert den Webservice "list_data".
- Webservice Aspekt
	Alle Webservice Aufrufe werden zum Base-Handler geroutet. Dort werden sie durch Methoden verarbeitet, deren Namen nach dem Schema __webservice__<webservicename>() aufgebaut
	sind. Die Methoden rufen ihrerseits den Webservice auf (separate Klasse) oder verarbeiten die Anfrage selbst. Schließlich wird eine Antwort zurückgegeben, die im Header
	"Access-Control-Allow-Origin" auf "*" setzt (wegen Cross-Origin-Resource-Sharing (CORS)) und im Payload einen JSON String mit zwei Attributen in der Wurzel enthält:
	"success" ist bool'sch und zeigt an, ob der Webservice korrekt gelaufen ist und "data" als String, in dem (wieder im JSON Format) die eigentliche Antwort des Webservice
	Aufrufs steckt.

